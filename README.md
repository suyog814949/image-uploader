This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Image Cropper

Its a simple CRA which uploads image to S3 bucket after cropping them in 4 different dimensions.
It takes valid input image which should be 1024*1024 in dimension and then on successfull submission of it, it creates 4 new images form cropping input image in different dimension and finally uploads it on S3

### Technologies used
1. React
2. Typescript
3. Styled components
4. AWS S3 integration


In the project directory, you can run:
### `yarn start`



