import { IImageDimension } from "../App";

const getImageParams=(image:any,dimension:IImageDimension)=>{
  // get width and height of output image
  const {height:opImageHeight, width:opImageWidth} = dimension;

  const {naturalWidth: imageWidth, naturalHeight: imageHeight} = image;
  
  // calculate the position to draw the image
  const opX = (opImageWidth - imageWidth)/5;
  const opY = (opImageHeight - imageHeight)/5;

  return {
    imageHeight,
    imageWidth,
    opImageHeight,
    opImageWidth,
    opX,
    opY
  } 
}

export interface ICropedResponse{
  image:any,
  canvasePath:string;
  dimension:IImageDimension
}

const crop=(image:any, dimension:IImageDimension):Promise<ICropedResponse>=>{

  const reader = new FileReader();
      
  // we return a Promise that gets resolved with our canvas element
  return new Promise(resolve => {

    reader.readAsDataURL(image);
    reader.onload = function (e:any) {

      const inputImage = new Image();

      inputImage.onload = () => {

          const {
            opImageHeight,
            opImageWidth,
            opX,
            opY
          } =getImageParams(inputImage,dimension)
          
          // create a canvas that will present the output image
          const canvas = document.createElement('canvas');
          const canvasCtx:any = canvas.getContext('2d');

          // setting O/P image dimensions
          canvas.width = opImageWidth;
          canvas.height = opImageHeight;

          // O/P image on canvas from computed axis points 
          
          canvasCtx.drawImage(inputImage, opX, opY);
            resolve({
              image,
              canvasePath:canvas.toDataURL(image.type),
              dimension:{
                width:opImageWidth,
                height:opImageHeight
              }
            });
      };

      // start loading image
      inputImage.src = e.target.result;

    }

  })
  
}


export default crop