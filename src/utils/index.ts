import { ICropedResponse } from "./crop";
import S3FileUpload from 'react-s3';
import SECRETS from "../secrets"

export const dataURItoBlob=(dataURI:string)=> {
  const binary = atob(dataURI.split(',')[1]);
  let array = [];
  for(let i = 0; i < binary.length; i++) {
      array.push(binary.charCodeAt(i));
  }
  return new Blob([new Uint8Array(array)], {type: 'image/jpeg'});
}


export const uploadToS3=(canvasImages:ICropedResponse[]):Promise<any>[]=>{

  const uniqueId = new Date().valueOf();
  const config = {
      bucketName: SECRETS.BUCKET_NAME,
      region: SECRETS.REGION,
      accessKeyId: SECRETS.ACCESS_KEY_ID,
      secretAccessKey: SECRETS.SECRET_ACCESS_KEY,
  }

  return canvasImages.map((canvasImage)=>{
      const { image, canvasePath, dimension } = canvasImage
      const blob = dataURItoBlob(canvasePath);

      // Image file with new name- uniqueId with dimension appended with image extension 
      const fileOfBlob = new File([blob], `${uniqueId}-${dimension.height}*${dimension.width}-${image.type.split("/"[1])}`);

    return new Promise((resolve,reject)=>{

        S3FileUpload
          .uploadFile(fileOfBlob, config)
          .then((data:any) => resolve(data))
          .catch((err:any) => reject(err))
    })

  })

}
