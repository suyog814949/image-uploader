import React from 'react';
import './App.css';
import styled from 'styled-components'
import Upload from './upload'
import crop, { ICropedResponse } from './utils/crop';
import Preview from './components/preview';
import { uploadToS3 } from './utils';
import Loader from './components/loader';

export interface IImageDimension { height:number,width:number } 
export interface IUploadConfig {
  [types:string]:{ [properties:string]:string|number } | string[],
} 
interface IFilesUploadStatus{
  code: null  // - not inititated
        | 1   // - inititated
        | 2   // - success
        | 3,   // - failed
  message?:null|string
}

const uploadConfig:IUploadConfig = {
  types:["image"],
  image:{
    height:1024,
    width:1024,
  }
}

const OutputImageDimensions:IImageDimension[] = [
  { width:755,height:450 },
  { width:450,height:365 },
  { width:365,height:212 },
  { width:380,height:380 },
]

const App:React.FC =()=>{

  const [previewItems, updatePreviewItems] = React.useState<null|any[]>(null)
  const [filesUploadStatus,upadteFilesUploadStatus] = React.useState<IFilesUploadStatus>({code:null})

  const cropAndUpload =  async(OutputImageDimensions:IImageDimension[],file:any)=>{
    // Initiate crop
    const imageCropPromises:Promise<ICropedResponse>[] = OutputImageDimensions.map((dimension:IImageDimension)=>crop(file,dimension));
    
    // Reset preview to `NULL` if any
    previewItems && updatePreviewItems(null)

    // Receive all cropped images
    const CanvasImages:ICropedResponse[] = await Promise.all(imageCropPromises)

    // Inititate Upload
    uplaodImages(CanvasImages)

  }

  //  Upload files to S3 bucket
  const uplaodImages=(CanvasImages:ICropedResponse[])=>{
    
    upadteFilesUploadStatus({code:1})

    const uplaodPromises = uploadToS3(CanvasImages)

    Promise.all(uplaodPromises).then((uploadedImages)=>{
      upadteFilesUploadStatus({code:2})
      // Show preview of uploaded images get uplaoded
      updatePreviewItems(uploadedImages)
    }).catch((error)=>{
      upadteFilesUploadStatus({code:3, message:error})
    })
  }

  return (
    <MainWrapper>
        <div>
            <h3>IMAGE CROPPER</h3><hr/>
        </div>
        <Upload config={uploadConfig} onValidFile={cropAndUpload.bind(null,OutputImageDimensions)} />

        {
          // Case of Uploaad - Initiated/Error-occurance
          (filesUploadStatus.code===1 || filesUploadStatus.code===3) && (
              <div style={{marginTop:'10px'}}>
              {
                // Show loader when - Uplaod is inititated
                filesUploadStatus.code===1 && (
                  <LoadingWrapper>
                    UpLoading images to AWS S3 bucket...
                    <Loader />
                  </LoadingWrapper>
                )
              }
              {
                // Loading Error
                filesUploadStatus.code===3 && (
                    <Error> {filesUploadStatus.message} </Error>
                )
              }
            </div>
          )
        }
        {
          // Preview items - till loading is happpening in background
          previewItems && (
            <div style={{width:'90%'}}>
              <Preview 
                title="Cropped images"
                items={(previewItems)} 
              />
            </div>   
          )
        }
    </MainWrapper>
  );
}

const LoadingWrapper = styled.div`
  align-items: center;
  display: flex;
  flex-direction: column;
`

const MainWrapper = styled.div`
  width:60% !important;
  display: flex;
  flex-direction: column;
  align-items: center;
  border:solid 1px grey;
  min-height:700px;
  margin: 0 auto;
  background:#ebfaff;
  margin-top:50px;
  padding: 10px 0px;
`


export const Error = styled.div`
    padding: 2px;
    margin-top: 8px;
    width: 100%;
    text-align: CENTER;
    font-size: 12px;
    border-radius: 4px;
    color: rgb(233, 66, 32);
    background: rgb(251, 217, 210);
`

export default App;
