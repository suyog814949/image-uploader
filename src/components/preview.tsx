import React from 'react';
import styled from 'styled-components'

interface IPreviewProps{
  items:any[],
  title:string
}

const Preview:React.FC<IPreviewProps> =({items, title})=>{
  console.log(items)
  const gallery = items.map((item:any)=>{
    return (
      <PreviewImgWrapper key={item.location} >
        <div>
          <PreviewImg src={item.location} />
        </div>
      </PreviewImgWrapper>
    )
  })

  return (
    <>
      <PreviewHeader> {title} </PreviewHeader>
      <hr/>
      <PreviewWrapper>
        { gallery }
      </PreviewWrapper>
    </>
  );
}

const PreviewHeader = styled.div`
  padding: 8px;
  border-radius: 5px;
  text-align: center;
  background: rgb(255,255,255);
  color: rgb(127,143,164);
  margin-top: 50px
`

const PreviewWrapper = styled.div`
  border-radius: 4px;
  border-radius: 5px;
`

const PreviewImg = styled.img``

const PreviewImgWrapper = styled.div`
  padding:10px;
  text-align:center;
  background: white;
  border-radius: 5px;
  border-bottom: dotted 1px lightgray;
  :last-of-type{
    border-bottom:none;
  }
`

export default Preview;
