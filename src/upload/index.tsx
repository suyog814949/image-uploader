import React from 'react';
import styled from 'styled-components'
import FileValidator, { IValidtionResponse } from "./validator"
import { IUploadConfig,Error } from '../App';
interface IUploadProps{
    config:IUploadConfig;
    onValidFile:(file:any)=>void;
}

const Upload: React.FC<IUploadProps> = (props) => {

    const { config, onValidFile } = props
    const fileRef = React.useRef(null)
    const [fileError, updatefileError] = React.useState<null|string>(null)
    const handleUpload: React.MouseEventHandler<HTMLButtonElement> = async(e) => {
        e.preventDefault();
        updatefileError(null)

        const file = fileRef && (fileRef as React.RefObject<any>).current.files[0]  
        // Validate file
        const {isValid, message}:IValidtionResponse = await FileValidator(file,config)
        
        if(!isValid){
            updatefileError(message)
            return
        }

        // passing valid FILE in callback
        onValidFile(file)

    };

    return (
        <MainWrapper>
            <input type="file" ref={fileRef} onChange={()=>{ updatefileError(null) }}  />
            <StyledButton onClick={handleUpload} >UPLOAD</StyledButton>
            {
                fileError && (
                    <Error>{ fileError }</Error>
                )
            }
        </MainWrapper>
    );
}

const MainWrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    width:300px;
    margin-top: 20px;
    padding: 10px;
    background: rgb(255,255,255);
    border-radius: 5px;
`

const StyledButton= styled.button`
    cursor:pointer;
    width: 40%;
    margin-top: 10px;
    padding: 5px;
    background: lightgrey;
    color: white;
    font-weight: 400;
    border: none;
    border-radius: 3px;
`

export default Upload;
