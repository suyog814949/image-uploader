export interface IValidtionResponse{
    isValid:boolean,
    message:null|string
}

export const FileValidator = async (file:any, config:any):Promise<IValidtionResponse>=>{

    // File check
    if(!file){
        // Mandatory validation response
        return { isValid:false, message:"Please select your file first" }
    }

    // File config check type for all file formats
    if(config.types.indexOf('*')>-1){
        return { isValid:true, message:null }
    }

    
    // Type checking
    const {types} = config
    const {type} = file
    if(!types.some((tempType:string)=>type.includes(tempType))){
        return { isValid:false, message:`Please select from following file types ${types.join(",")}` }
    }


    const reader = new FileReader();
    
    // If type is - Image
    if(types.indexOf("image")>-1 && config.image){
    
        const imageValConfig = config.image

        // Validates image sub-types i.e. - jpeg,png,svg,...
        if(imageValConfig.type){
            const imageType = type.split("/")[0]

            if(imageValConfig.type.indexOf(imageType)===-1){
                return {
                    isValid: false, 
                    message:`${imageValConfig.type.join(",")} is only allowed`
                }
            }
        }
    
        // Check for width and height
        if(imageValConfig.width || imageValConfig.height){
             const validationStateObj:IValidtionResponse =  await new Promise(resolve => {
                //Read the contents of Image File.
                reader.readAsDataURL(file);
                reader.onload = function (e:any) {
        
                    //Initiate the JavaScript Image object.
                    let image = new Image();
                    
                    //Set the Base64 string return from FileReader as source.
                    image.src = e.target.result;
                    
                    //Validate the File Height and Width.
                    image.onload = function(){
                        
                        let isValid = true, message=[]

                        // width check
                        if(imageValConfig.width && imageValConfig.width!==image.width)
                            message.push(`width=${imageValConfig.width}`) && (isValid=false)
    
                        // height check
                        if(imageValConfig.height && imageValConfig.height!==image.height)
                            message.push(`height=${imageValConfig.width}`) && (isValid=false)
                            

                        if(!isValid){
                            resolve({
                                isValid: false, 
                                message:`image with ${message[0]} ${message[1] && 'and '+message[1]+' '}is only allowed`
                            })
                        }else{
                            resolve({
                                isValid: true, 
                                message:null
                            })
                        }


                    }
                }
            })

            if(!validationStateObj.isValid){
                return validationStateObj
            }
        }



    }


    // Rules for PDF
    // Rules for Doc
    // Rules for ......



    // Default Response
    return { isValid:true, message:null }
}

export default FileValidator